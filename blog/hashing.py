from passlib.handlers.pbkdf2 import pbkdf2_sha256


class Hash:
    def __init__(self):
        pass

    def hash_password(self, password: str):
        return pbkdf2_sha256.hash(password)
