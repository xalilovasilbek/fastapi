import uvicorn
from fastapi import FastAPI
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


class Blog(BaseModel):
    title: str
    body: str
    published: bool


@app.get('/blog')
def index(limit=10, published: bool = True, sort: Optional[str] = None):
    if published:
        return {'data': f'{limit} published blogs from the db'}
    return {'data': f'{limit} blogs from the db'}


@app.get('/blog/unpublished')
def unpublished():
    return {'data': 'all unpublished blogs'}


@app.get('/blog/{b_id}')
def show(b_id: int):
    return {'about': b_id}


@app.get('/blog/{b_id}/comments')
def comments(b_id):
    return {'data': [1, 2]}


@app.post('/blog')
def create_blog(request: Blog):
    return {'data': f'Blog is created with title as {request.title}'}


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8000)
